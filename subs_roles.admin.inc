<?php

function subs_roles_settings_form($form, &$form_state) {
  $form = array();

  $form['subs_roles_status_assign'] = array(
    '#type' => 'select',
    '#title' => t('Default status subs to assign roles'),
    '#description' => t('Define default status subs to assign roles to subscribe user. Should be override by the subs type entity itself. If this status has change, it will revert roles previously assigned, including manually.'),
    '#options' => subs_status_options_list(),
    '#default_value' => variable_get('subs_roles_status_assign', ''),
  );
  
  return system_settings_form($form);
}
