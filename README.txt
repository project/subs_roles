Subs roles
----------

This module provide a basic and simple way to set roles to
users subscribes.

Its alter subs type entity to add a field
storing roles rid to attributes to users associated
to a subs, depending on the subs status.

When the triggered status is matched, roles defined
will be added to the users subcribe.

If subs status change and doesn't match status to trigger
for attribution, roles added will be removed.

In that way, you don't have to create rules each time
a new role/subs type have been created and wanted to
attribute them.

As it used hook_entity, it deals with :
- CRUD from admin back-end
- cron job

But this module have been develop for simple use case
and have some limits :
- exclude default roles anonymous and authenticated user
- a role could be used by only one subs type
  and be added/removed only through subs status. Otherwise,
  this is your own risks (using UI for e.g).

- only one subs status should be trigger

In other word, if it doesn't match to your needs,
rules module (http://drupal.org/project/rules) could be a solution 
as it's integrate by subs module itself.

NOTE : if further users have a subscription
and its subs type have defined roles to assign have changed,
it doesn't revert user roles previously assigned.

-----------
REQUIREMENT
-----------
- subs

-------
INSTALL
-------

- Enable module like any other contrib modules.

- Go to admin/config/workflow/subs/roles and define
  default subs status to trigger for adding roles 
  to users subscribe (or on contrary to remove).

- Add/edit a subs type and in "roles assign workflow" fieldset,
  select which roles must be added/removed and status subs to trigger
  (if no status define, will used default previously set).

- Go to admin/people/permissions and define permissions
  for allowing roles to configure settings