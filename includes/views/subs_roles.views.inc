<?php

/**
 * Implements hook_views_data_alter()
 */
function subs_roles_views_data_alter(&$data) {

  $data['subs_type']['roles_assign'] = array(
    'title' => t('Roles to assign'),
    'help' => t('Serialized'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['subs_type']['roles_assign_list'] = array(
    'title' => t('Roles to assign'),
    'help' => t('List'),
    'field' => array(
      'real field' => 'roles_assign',
      'handler' => 'SubsRolesViewsHandlerFieldRoleAssign',
    ),
  );
}