<?php

/**
 * @file
 * A views field handler. See subs_role_views_data_alter().
 */

/**
 * Views field handler for display a list of roles to assigned
 */
class SubsRolesViewsHandlerFieldRoleAssign extends views_handler_field {

  /**
   * Implements views_handler_field::render().
   */
  function render($values) {
    $output = '';

    $roles = unserialize($values->subs_type_roles_assign);
    if ($roles) {

      $items = array();
      foreach ($roles as $rid => $rolename) {
        $role = user_role_load($rid);
        if ($role) {
          $items[] = $role->name;
        } 
      }

      if ($items) {
        $output = theme('item_list', array('items' => $items));
      }
    }

    return $output;
  }
}